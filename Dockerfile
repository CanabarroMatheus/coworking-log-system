FROM openjdk

WORKDIR /

EXPOSE 8081

ADD ./build/libs/logAPI-0.0.1-SNAPSHOT.jar /
RUN sh -c 'touch logAPI-0.0.1-SNAPSHOT.jar'

ENTRYPOINT ["java", "-jar", "logAPI-0.0.1-SNAPSHOT.jar"]