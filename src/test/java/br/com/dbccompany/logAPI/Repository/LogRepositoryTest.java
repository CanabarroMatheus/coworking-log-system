package br.com.dbccompany.logAPI.Repository;

import br.com.dbccompany.logAPI.Collection.LogCollection;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;

import static org.junit.jupiter.api.Assertions.assertTrue;

@DataMongoTest
public class LogRepositoryTest {
    @Autowired
    private LogRepository repository;

    @Test
    public void salvarNovoLogEBuscarPorCodigo(){
        LogCollection testLog = new LogCollection();
        testLog.setCodigo("teste");
        testLog.setData("teste");
        testLog.setTipo("teste");
        testLog.setDescricao("teste");
        repository.save(testLog);
        assertTrue( repository.findAllByCodigo("teste").size()>0 );
    }

    @Test
    public void salvarNovoLogEBuscarPorTipo(){
        LogCollection testLog = new LogCollection();
        testLog.setCodigo("teste");
        testLog.setData("teste");
        testLog.setTipo("WARN");
        testLog.setDescricao("teste");
        repository.save(testLog);
        assertTrue( repository.findAllByTipo("WARN").size()>0 );
    }
}
