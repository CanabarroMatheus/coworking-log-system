package br.com.dbccompany.logAPI.Controller;

import br.com.dbccompany.logAPI.DTO.LogDTO;
import br.com.dbccompany.logAPI.Exception.ArgumentosInvalidos;
import br.com.dbccompany.logAPI.Exception.LogNaoEncontrado;
import br.com.dbccompany.logAPI.Service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/logger")
public class LogController {

    @Autowired
    private LogService service;

    @GetMapping("/trazer/todos")
    public List<LogDTO> trazerTodos() {
        return this.service.findAll();
    }

    @PostMapping("/salvar")
    public ResponseEntity<LogDTO> salvar(@RequestBody LogDTO erro) {
        try {
            return new ResponseEntity<LogDTO>(service.insert(erro), HttpStatus.ACCEPTED);
        } catch (ArgumentosInvalidos argumentosInvalidos) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/trazer/todos/codigo/{codigo}")
    public ResponseEntity<List<LogDTO>> encontrarPorCodigo(@PathVariable String codigo) {
        try {
            return new ResponseEntity<List<LogDTO>>(this.service.buscarPorCodigo(codigo), HttpStatus.ACCEPTED);
        } catch (LogNaoEncontrado e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/trazer/todos/tipo/{tipo}")
    public ResponseEntity<List<LogDTO>> encontrarPorTipo(@PathVariable String tipo) {
        try {
            return new ResponseEntity<List<LogDTO>>(this.service.buscarPorTipo(tipo), HttpStatus.ACCEPTED);
        } catch (LogNaoEncontrado e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

}
