package br.com.dbccompany.logAPI.Service;

import br.com.dbccompany.logAPI.Collection.LogCollection;
import br.com.dbccompany.logAPI.DTO.LogDTO;
import br.com.dbccompany.logAPI.Exception.ArgumentosInvalidos;
import br.com.dbccompany.logAPI.Exception.LogNaoEncontrado;
import br.com.dbccompany.logAPI.Repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LogService {

    @Autowired
    private LogRepository repository;
    private MongoTemplate mongoTemplate;

    public List<LogDTO> findAll(){
        return this.convertListToDTO( repository.findAll() );
    }

    public LogDTO insert(LogDTO erroDTO) throws ArgumentosInvalidos {
        try{
            LogCollection erroLog = erroDTO.convert();
            return new LogDTO(this.repository.insert(erroLog));
        }catch(Exception e){
            System.err.println("Erro ao inserir");
            throw new ArgumentosInvalidos();
        }
    }

    public List<LogDTO> buscarPorCodigo(String codigo) throws LogNaoEncontrado {
        try{
            return this.convertListToDTO( repository.findAllByCodigo(codigo));
        }catch(Exception e){
            System.err.println("Erro ao realizar busca");
            throw new LogNaoEncontrado();
        }
    }

    public List<LogDTO> buscarPorTipo( String tipo ) throws LogNaoEncontrado {
        try{
            return this.convertListToDTO( repository.findAllByTipo(tipo));
        }catch(Exception e){
            System.err.println("Erro ao realizar busca");
            throw new LogNaoEncontrado();
        }
    }

    private List<LogDTO> convertListToDTO(List<LogCollection> logCollections){
        ArrayList<LogDTO> itensNovo = new ArrayList<LogDTO>();
        for(LogCollection logCollection : logCollections){
            itensNovo.add(new LogDTO(logCollection));
        }
        return itensNovo;
    }

}
