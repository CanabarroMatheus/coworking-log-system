package br.com.dbccompany.logAPI.Exception;

public class ArgumentosInvalidos extends LogException {
    public ArgumentosInvalidos() {
        super("Argumentos invalidos para cadastrar erro");
    }
}
