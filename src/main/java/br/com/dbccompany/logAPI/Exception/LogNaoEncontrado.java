package br.com.dbccompany.logAPI.Exception;

public class LogNaoEncontrado extends LogException {
    public LogNaoEncontrado() {
        super("Não foi possível realizar a busca!");
    }
}
