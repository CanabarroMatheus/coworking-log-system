package br.com.dbccompany.logAPI.Repository;

import br.com.dbccompany.logAPI.Collection.LogCollection;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LogRepository extends MongoRepository<LogCollection, String> {
    List<LogCollection> findAllByCodigo(String codigo);
    List<LogCollection> findAllByTipo(String tipo);
}
