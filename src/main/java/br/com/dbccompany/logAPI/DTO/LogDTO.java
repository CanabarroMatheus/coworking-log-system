package br.com.dbccompany.logAPI.DTO;

import br.com.dbccompany.logAPI.Collection.LogCollection;

public class LogDTO {
    private String data;
    private String tipo;
    private String codigo;
    private String descricao;

    public LogDTO(){}

    public LogDTO(LogCollection erro){
        this.data = erro.getData();
        this.tipo = erro.getTipo();
        this.codigo = erro.getCodigo();
        this.descricao = erro.getDescricao();
    }

    public LogCollection convert(){
        LogCollection erro = new LogCollection();
        erro.setData(this.data);
        erro.setTipo(this.tipo);
        erro.setCodigo(this.codigo);
        erro.setDescricao(this.descricao);
        return erro;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
